Dear Sir/Madam,

I hope this email finds you well. My name is Sara Khan, and I am currently a first-year student pursuing a Master of Pharmacy (M. Pharm) with a specialization in Pharmacology at Integral University in India. I am writing to express my strong interest in pursuing a Ph.D. in pharmacology at Duke University after the completion of my master's degree.

As an aspiring international student, I am eager to gather detailed information about the admission process and program specifics. I kindly request your assistance in providing the following details:

**Admission Requirements and Eligibility Criteria:**

a. Are there any standardized exams (e.g., GRE, TOEFL, IELTS) that international applicants must take?

b. What are the minimum score requirements for these exams?

c. What documents are required for the application process?

**Application Procedure:**

a. Could you please outline the step-by-step process for applying?

b. Are their specific deadlines for submitting applications?

**Financial Information:**

a. What is the fee structure for the Ph.D. program in Pharmacology for international students?

b. Are stipends or any other forms of financial assistance available for PhD students?

c. If so, what is the procedure for availing of these opportunities?

I understand that some of this information may be available on the university's official website, but I believe direct communication will provide a more comprehensive understanding. Your guidance in this matter is highly appreciated, and I am grateful for your assistance.

Thank you for your time, and I look forward to your response.

Sincerely,

Sara Khan

+91-8707811192

